//
//  HomeVC.swift
//  500PX
//
//  Created by Magdi Zamel on 3/4/17.
//  Copyright © 2017 Magdi Zamel. All rights reserved.
//

import Foundation
import UIKit
import PageMenu

class HomeVC: UIViewController ,CAPSPageMenuDelegate , NVActivityIndicatorViewable{
   
   
   @IBOutlet weak var conitainer: UIView!
   
   var tabs : CAPSPageMenu!
   var controllersArray :[UIViewController]!
   var parameters: [CAPSPageMenuOption]!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      pageMenuConfigration()
   }

   
   override func viewDidAppear(_ animated: Bool) {
      super.viewDidAppear(animated)
      // Initialize page menu with view controllers array, frame, and optional parameters
      if tabs == nil{
      tabs = CAPSPageMenu(viewControllers: controllersArray, frame: conitainer.bounds , pageMenuOptions: parameters)
      self.conitainer.addSubview(self.tabs.view)
      tabs.moveToPage(0)

      }
   }
   
   
   
   func pageMenuConfigration(){
      
      parameters = [
         .scrollMenuBackgroundColor(UIColor.init(red: 283/255, green: 283/255, blue: 283/255, alpha: 1)),
         .viewBackgroundColor(UIColor.groupTableViewBackground),
         .selectionIndicatorColor(UIColor.init(red: 0, green: 153/255, blue: 229/255, alpha: 1)),
         .unselectedMenuItemLabelColor(UIColor.black),
         .selectedMenuItemLabelColor(UIColor.init(red: 0, green: 153/255, blue: 229/255, alpha: 1)),
         .bottomMenuHairlineColor(UIColor.darkGray),
         .menuItemFont(UIFont(name: "trebuchet ms", size: 13.0)!),
         .menuHeight(40.0),
         .menuItemWidthBasedOnTitleTextWidth (true),
         .menuMargin(20),
         .centerMenuItems(true)
      ]
      
      controllersArray = [UIViewController]()
      for element in  categories  {
         let vc = storyboard!.instantiateViewController(withIdentifier: "PageVC") as! PageVC
         vc.title = " \(element.1) "
         vc.category = element.1
         controllersArray.append(vc)
      }
      


   }
   
}
