//
//  User.swift
//  500PX
//
//  Created by Magdi Zamel on 3/4/17.
//  Copyright © 2017 Magdi Zamel. All rights reserved.
//

import Foundation
class User{
    let fullName :String
    var userPicUrl :URL?
    let userId:Int
    
   private let fullNameKey  = "fullname"
   private let userPicUrlKey = "userpic_url"
   private let userIdKey = "id"
    
    
    init(data:NSDictionary){
        
        userId  = data.getValueOf(key:userIdKey, callback: -1)
        fullName = data.getValueOf(key:fullNameKey, callback: " ")
        userPicUrl =  URL(string :data.getValueOf(key:userPicUrlKey, callback: " "))
        
    }
    
}
