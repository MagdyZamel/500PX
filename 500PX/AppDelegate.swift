//
//  AppDelegate.swift
//  500PX
//
//  Created by Magdi Zamel on 5/4/17.
//  Copyright © 2017 Magdi Zamel. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    private func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }


}

