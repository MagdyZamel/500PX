//
//  tabBarController.swift
//  500PX
//
//  Created by Magdi Zamel on 3/9/17.
//  Copyright © 2017 Magdi Zamel. All rights reserved.
//

import UIKit

class tabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(self.requestFailed), name: notificationName, object: nil)
    }
    func networkNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.requestFailed), name: notificationName, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func requestFailed()  {
        print (" Error With Your Connection do your favourite behaviour \n  Drag to reload ")
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
