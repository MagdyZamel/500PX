//
//  SearchVC.swift
//  500PX
//
//  Created by Magdi Zamel on 3/7/17.
//  Copyright © 2017 Magdi Zamel. All rights reserved.
//

import Foundation
import UIKit


class SearchVC : UIViewController ,UITextFieldDelegate, UITableViewDataSource , UITableViewDelegate{
    
    
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var list: UITableView!
    
    
    var pageNubember :Int!
    
    var photosList = [Photo]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        list.rowHeight = UITableViewAutomaticDimension
        list.estimatedRowHeight = 470
        pageNubember = 1
        NotificationCenter.default.addObserver(self, selector: #selector(self.requestFailed), name: notificationName, object: nil)

    }
    
    
    // MARK: - UItableViewDataSource protocol
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return photosList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ThumbCell", for: indexPath ) as! ThumbCell
        
        //Set cell content
        cell.uesrName.text = self.photosList[indexPath.row].owner.fullName
        cell.disPhoto.text = self.photosList[indexPath.row].description
        cell.namePhoto.text = self.photosList[indexPath.row].name
        cell.photoTokenTime.text = self.photosList[indexPath.row].takenAt
        cell.votesCount.text = "\(self.photosList[indexPath.row].votesCount) people "
        
        
        //Set cell images
        cell.photo.kf.setImage(with: self.photosList[indexPath.row].url)
        cell.uesrImage.kf.setImage(with: self.photosList[indexPath.row].owner.userPicUrl)
        
        return cell
        
    }
    
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        //Bottom Refresh
        if scrollView.tag == 1{
            if ((scrollView.contentOffset.y + scrollView.frame.size.height)  >= scrollView.contentSize.height )
            {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
                
                pageNubember = pageNubember + 1
                RequestManager.defaultManager.getPhotesWithType(type: .searchWithKey, category: nil, key: searchField.text!,pagenumber: pageNubember) { (photosModel) in
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.photosList  = self.photosList + photosModel.photos
                    self.list.reloadData()
                }
            }
        }
    }
    
    func keyboardConfiguration(){
        // Gesture to dismissKeyboard
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SearchVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        SearchTaped(key: textField.text!)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        view.endEditing(true)
        return true
    }
    func dismissKeyboard()  {
        view.endEditing(true)
        
    }
    func SearchTaped(key :String){
        pageNubember = 1
        RequestManager.defaultManager.getPhotesWithType(type: .searchWithKey, category: nil, key: key,pagenumber: pageNubember) { (photosModel) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.photosList  = photosModel.photos
            self.list.reloadData()
            
            if photosModel.photos.count == 0{
                self.list.isHidden = true
                self.presentAlert(title:nil,message: "No Photos founded")
                
                return
            }
            
            self.list.isHidden = false
        }
    }
    
    func requestFailed(){
        pageNubember = photosList.count / 20
        
    }

    deinit {
        notificationCenter.removeObserver(self)
    }
    
    
}
