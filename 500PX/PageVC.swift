    //
    //  ViewController.swift
    //  500PX
    //
    //  Created by Magdi Zamel on 3/4/17.
    //  Copyright © 2017 Magdi Zamel. All rights reserved.
    //
    
    import UIKit
    import Kingfisher
    
    class PageVC: UIViewController ,UITableViewDataSource , UITableViewDelegate {
        
        @IBOutlet weak var list: UITableView!
        @IBOutlet weak var loader: NVActivityIndicatorView!
        @IBOutlet weak var shadowView: UIView!
        
        var photosList = [Photo]()
        var pageNubember :Int!
        var refresher:UIRefreshControl!
        var category :String!

        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            setTableConfigration()
            networkConfigrationg()
            getDataWith(page: pageNubember)
        }
        
        func setTableConfigration(){
            
            loader.startAnimating()

            list.rowHeight = UITableViewAutomaticDimension
            list.estimatedRowHeight = 470
            pageNubember = 1
            refresher  = UIRefreshControl()
            refresher.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
            list.addSubview(refresher)

        }
        func networkConfigrationg() {
            NotificationCenter.default.addObserver(self, selector: #selector(self.requestFailed), name: notificationName, object: nil)
        }
        
        
        // MARK: - UItableViewDataSource protocol
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            return photosList.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ThumbCell", for: indexPath ) as! ThumbCell
            
            //Set cell content
            cell.uesrName.text = self.photosList[indexPath.row].owner.fullName
            cell.disPhoto.text = self.photosList[indexPath.row].description
            cell.namePhoto.text = self.photosList[indexPath.row].name
            cell.photoTokenTime.text = self.photosList[indexPath.row].takenAt
            cell.votesCount.text = "\(self.photosList[indexPath.row].votesCount) people "
            
            
            //Set cell images
            cell.photo.kf.setImage(with: self.photosList[indexPath.row].url,placeholder: UIImage(named: "placeHolder"))
            cell.uesrImage.kf.setImage(with: self.photosList[indexPath.row].owner.userPicUrl,placeholder: UIImage(named: "placeHolder"))
            
            return cell
            
        }
        
        func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
            if scrollView.tag == 1{
                if ((scrollView.contentOffset.y + scrollView.frame.size.height)  >= scrollView.contentSize.height )
                {
                    pageNubember = pageNubember + 1
                    getDataWith(page: pageNubember)
                }
            }
            
        }
        
        // MARK: - Download data from the api
        func getDataWith(page:Int){
            RequestManager.defaultManager.getPhotesWithType(type: .getAllPhotos, category: self.category, key: nil,pagenumber: page) { (photosModel) in
                self.photosList  = self.photosList + photosModel.photos
                self.list.reloadData()
                self.refresher.endRefreshing()
                if self.loader.isAnimating {
                self.loader.stopAnimating()
                self.list.isHidden = false
                }
            }
        }
        func requestFailed(){
            self.refresher.endRefreshing()
            pageNubember = photosList.count / 20
            if self.loader.isAnimating {
                self.loader.stopAnimating()
                self.list.isHidden = false

            }
        }
        
        func refresh(){
            getDataWith(page: pageNubember)
        }
        
        deinit {
            notificationCenter.removeObserver(self)
        }

    }
    
