//
//  RequestManager.swift
//  500PX
//
//  Created by Magdi Zamel on 3/4/17.
//  Copyright © 2017 Magdi Zamel. All rights reserved.
//

import Foundation
import Alamofire


class RequestManager{
    static let defaultManager = RequestManager()
    private var request: Alamofire.Request?

    private init (){}
    
    
    func getPhotesWithType( type:MethodType, category:String?, key :String?,pagenumber:Int,compilition : @escaping (( _ photosModel: PhotosModel) -> Void)){
        var key = key
        var category = category
        var  link:String!
        switch type{
        case .getAllPhotos:
            category = category!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            link = "\(BASEDOMAIN)/v1/photos?only=\(category!)&sort=created_at&image_size=3&page=\(pagenumber)&consumer_key=\(CONSUMERKEY)"
        case .searchWithKey:
            key = key!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
            link = "\(BASEDOMAIN)/v1/photos/search?term=\(key!)&image_size=3&page=\(pagenumber)&consumer_key=\(CONSUMERKEY)"
            request?.cancel()
        }
    
        request = Alamofire.request(link).responseJSON { response in
            switch response.result {
            case .success(let value):
                compilition(PhotosModel(data: value ))
            case .failure:
                NotificationCenter.default.post(name: notificationName, object: nil)
            }
    }
    }




}
