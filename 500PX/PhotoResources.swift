//
//  Photo.swift
//  500PX
//
//  Created by Magdi Zamel on 3/4/17.
//  Copyright © 2017 Magdi Zamel. All rights reserved.
//

import Foundation

class PhotosModel {
   private let photosKey = "photos"
    var photos  = [Photo]()
    init(data:Any){
        if let data = data as? NSDictionary{
            let data : [NSDictionary] = data.getValueOf(key:photosKey, callback: [])
            for element in data{
            self.photos.append(Photo(data: element))
                
            }
        }
    }
}


class Photo {
    let photoId:Int
    let name:String
    let description:String
    var url:URL?
    var takenAt:String!
    
    let votesCount:Int
    let category:Int
    
    let owner :User!
    
    
   private let photoIdKey = "id"
   private let nameKey = "name"
   private let descriptionKey = "description"
   private let takenAtKey = "taken_at"
   private let votesCountKey = "votes_count"
    private let ownerKey = "user"
    private let urlKey = "image_url"
    private let categoryKey = "category"
    

    
    init(data:NSDictionary){
        photoId  = data.getValueOf(key:photoIdKey, callback: -1)
        name = data.getValueOf(key: nameKey, callback: " ")
        description = data.getValueOf(key:descriptionKey, callback: " ")
        url = URL(string:data.getValueOf(key:urlKey, callback: ""))
        votesCount = data.getValueOf(key:votesCountKey, callback: 0)
        owner = User(data: data.getValueOf(key:ownerKey, callback: [:]))
        category = data.getValueOf(key:categoryKey, callback: 0)
        takenAt = self.convert(customDate:data.getValueOf(key:takenAtKey, callback: " "))
    }
    func convert(customDate : String) ->String{
        if customDate == " " {
            return ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
        let date = dateFormatter.date(from: customDate)
        dateFormatter.dateFormat = "EEEE, MMMM d, yyyy"
        dateFormatter.dateStyle = DateFormatter.Style.full
                if let date = date {
            let newDate = dateFormatter.string(from: date)
            return newDate
        }
        return ""
    }
}

