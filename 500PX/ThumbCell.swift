//
//  ThumbCell.swift
//  500PX
//
//  Created by Magdi Zamel on 3/4/17.
//  Copyright © 2017 Magdi Zamel. All rights reserved.
//

import Foundation
import UIKit
class ThumbCell: UITableViewCell {
    
    @IBOutlet weak var uesrImage: UIImageView!
    @IBOutlet weak var uesrName: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var namePhoto: UILabel!
    @IBOutlet weak var disPhoto: UILabel!
    @IBOutlet weak var photoTokenTime: UILabel!
    @IBOutlet weak var votesCount: UILabel!
    
    @IBOutlet weak var saveThumb: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        DispatchQueue.main.async {
            self.uesrImage.layer.cornerRadius = self.uesrImage.frame.size.height/2
            self.uesrImage.clipsToBounds = true
  
        }
    
    }
    
}
